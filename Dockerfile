FROM php:7.1-cli

RUN apt-get update && apt-get install -y git curl unzip

RUN pecl install xdebug-2.6.0

RUN php -r "copy('https://getcomposer.org/composer.phar', '/usr/local/bin/composer');" && chmod +x /usr/local/bin/composer

RUN apt-get clean; \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*;

WORKDIR /var/www
