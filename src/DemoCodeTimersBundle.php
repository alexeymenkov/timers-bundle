<?php


namespace DemoCode\TimersBundle;


use DemoCode\TimersBundle\DependencyInjection\Compiler\TimerSettingsPass;
use Symfony\Component\DependencyInjection\Compiler\PassConfig;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class DemoCodeTimersBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        $container->addCompilerPass(new TimerSettingsPass(), PassConfig::TYPE_BEFORE_REMOVING);
    }
}