<?php


namespace DemoCode\TimersBundle;


class TimerEvent
{
    /**
     * @var float
     */
    private $start;

    /**
     * @var float
     */
    private $end;

    /**
     * @var array
     */
    private $params = [];

    /**
     * @var \DateTime
     */
    private $startDate;

    function __construct()
    {
        $this->start = microtime(true);
        $tz = new \DateTimeZone(date_default_timezone_get() ?: 'UTC');
        /** @var \DateTime $startDate */
        $startDate =\DateTime::createFromFormat('U.u', sprintf('%.6F', $this->start), $tz);
        $this->startDate = $startDate;
        $this->startDate->setTimezone($tz);
    }

    public function stop(): bool
    {
        if ($this->end) {
            return false;
        }

        $this->end = microtime(true);

        return true;
    }

    public function getDuration(): ?float
    {
        if (!$this->end) {
            return null;
        }

        return $this->end - $this->start;
    }

    public function getStartDate(): \DateTime
    {
        return $this->startDate;
    }

    /**
     * @param array $params
     */
    public function setParams(array $params)
    {
        $this->params = $params;
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return $this->params;
    }

    public function addParam($name, $value): void
    {
        $this->params[$name] = $value;
    }

    public function isStarted(): bool
    {
        return $this->end === null;
    }
}
