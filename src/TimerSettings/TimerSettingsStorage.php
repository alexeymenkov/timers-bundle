<?php


namespace DemoCode\TimersBundle\TimerSettings;


interface TimerSettingsStorage
{
    public function save(TimerSettings $timerSettings): bool;

    public function hasSettings(\ReflectionMethod $reflectionMethod): bool;

    public function getSettings(\ReflectionMethod $reflectionMethod): TimerSettings;
}