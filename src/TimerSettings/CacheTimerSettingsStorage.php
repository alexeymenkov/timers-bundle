<?php


namespace DemoCode\TimersBundle\TimerSettings;


use Assert\Assertion;
use Doctrine\Common\Cache\Cache;

class CacheTimerSettingsStorage implements TimerSettingsStorage
{
    /**
     * @var Cache
     */
    private $cache;

    /**
     * @param Cache $cache
     */
    public function __construct(Cache $cache)
    {
        $this->cache = $cache;
    }

    public function save(TimerSettings $timerSettings): bool
    {
        return $this->cache->save($timerSettings->getIdentifier(), $timerSettings);
    }

    /**
     * @param \ReflectionMethod $reflectionMethod
     * @return bool
     */
    public function hasSettings(\ReflectionMethod $reflectionMethod): bool
    {
        return $this->cache->contains(TimerSettings::createIdentifier($reflectionMethod));
    }

    /**
     * @param \ReflectionMethod $reflectionMethod
     * @return TimerSettings
     */
    public function getSettings(\ReflectionMethod $reflectionMethod): TimerSettings
    {
        $settings = $this->cache->fetch(TimerSettings::createIdentifier($reflectionMethod));

        Assertion::isInstanceOf($settings, TimerSettings::class);

        return $settings;
    }
}