<?php


namespace DemoCode\TimersBundle\TimerSettings;


use DemoCode\TimersBundle\Timer;

final class TimerSettings implements \Serializable
{
    /**
     * @var \ReflectionMethod
     */
    private $reflection;

    /**
     * @var string
     */
    private $group;

    /**
     * @var array
     */
    private $params = [];

    /**
     * @param \ReflectionMethod $reflection
     * @param string $group
     * @param array $params
     */
    public function __construct(\ReflectionMethod $reflection, string $group = Timer::DEFAULT_GROUP, array $params = [])
    {
        $this->reflection = $reflection;
        $this->group = $group;
        $this->params = $params;
    }

    public function getIdentifier(): string
    {
        return self::createIdentifier($this->reflection);
    }

    public static function createIdentifier(\ReflectionMethod $method): string
    {
        return sprintf("%s:%s", $method->class, $method->name);
    }

    public function getReflection(): \ReflectionMethod
    {
        return $this->reflection;
    }

    public function getGroup(): string
    {
        return $this->group;
    }

    public function getParams(): array
    {
        return $this->params;
    }

    /**
     * @return string|void
     */
    public function serialize()
    {
        return serialize([
            "class" => $this->reflection->class,
            "method" => $this->reflection->name,
            "group" => $this->group,
            'params' => $this->params,
        ]);
    }

    /**
     * @param string $serialized
     */
    public function unserialize($serialized)
    {
        $data = unserialize($serialized);

        $this->reflection = new \ReflectionMethod($data["class"], $data["method"]);
        $this->group = $data["group"];
        $this->params = $data["params"];
    }
}