<?php
declare(strict_types=1);

namespace DemoCode\TimersBundle\PostProcessor;


use Assert\Assertion;

class PostProcessorRegistry
{
    /**
     * @var array
     */
    private $processors = [];

    /**
     * @param array $postProcessors
     */
    public function __construct(array $postProcessors)
    {
        foreach ($postProcessors as $id => $processor) {
            $this->addProcessor($id, $processor);
        }
    }

    /**
     * @param string $id
     * @param PostProcessor $processor
     */
    private function addProcessor(string $id, PostProcessor $processor): void
    {
        $this->processors[$id] = $processor;
    }

    /**
     * @param string $id
     * @return bool
     */
    public function hasProcessor(string $id)
    {
        return isset($this->processors[$id]);
    }

    /**
     * @param string $id
     * @throws \InvalidArgumentException
     * @return PostProcessor
     */
    public function getProcessor(string $id)
    {
        Assertion::keyExists($this->processors, $id);

        return $this->processors[$id];
    }
}