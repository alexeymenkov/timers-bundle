<?php


namespace DemoCode\TimersBundle\PostProcessor;


use DemoCode\TimersBundle\TimerEvent;

interface PostProcessor
{
    /**
     * @param TimerEvent $event
     * @param array $arguments
     * @param mixed $methodResult
     */
    public function process(TimerEvent $event, array $arguments, $methodResult): void;
}