<?php


namespace DemoCode\TimersBundle\PostProcessor;


use DemoCode\TimersBundle\TimerEvent;

class DelegatingPostProcessor implements PostProcessor
{
    /**
     * @var PostProcessor[]
     */
    private $processors;

    /**
     * @var PostProcessor[]
     */
    public function __construct(array $processors)
    {
        foreach ($processors as $processor) {
            $this->addProcessor($processor);
        }
    }

    /**
     * @param TimerEvent $event
     * @param array $arguments
     * @param mixed $methodResult
     */
    public function process(TimerEvent $event, array $arguments, $methodResult): void
    {
        foreach ($this->processors as $processor) {
            $processor->process($event, $arguments, $methodResult);
        }
    }

    private function addProcessor(PostProcessor $processor)
    {
        $this->processors[] = $processor;
    }
}