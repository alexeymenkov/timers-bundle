<?php


namespace DemoCode\TimersBundle\DependencyInjection\Compiler;


use DemoCode\TimersBundle\DependencyInjection\Configuration;
use DemoCode\TimersBundle\Timer;
use DemoCode\TimersBundle\TimerSettings\TimerSettings;
use DemoCode\TimersBundle\TimerSettings\TimerSettingsStorage;
use Symfony\Component\Config\Definition\Processor;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class TimerSettingsPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        $config = $this->getBundleConfig($container);

        /** @var TimerSettingsStorage $storage */
        $storage = $container->get(TimerSettingsStorage::class);

        foreach ($config["timers"] as $className => $methods) {
            foreach ($methods as $methodName => $settings) {
                if (!$settings["enabled"]) {
                    continue;
                }

                $group = $settings["group"] ?? Timer::DEFAULT_GROUP;
                $params = $settings["params"] ?? [];
                $ts = new TimerSettings(new \ReflectionMethod($className, $methodName), $group, $params);
                $storage->save($ts);
            }
        }
    }

    private function getBundleConfig(ContainerBuilder $container): array
    {
        $configs = $container->getExtensionConfig("demo_code_timers");

        return (new Processor())->processConfiguration(new Configuration(), $configs);
    }
}