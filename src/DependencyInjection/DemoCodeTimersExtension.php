<?php

namespace DemoCode\TimersBundle\DependencyInjection;

use DemoCode\TimersBundle\Aop\Interceptor\TimerInterceptor;
use DemoCode\TimersBundle\Aop\Pointcut\TimerPointcut;
use DemoCode\TimersBundle\PostProcessor\PostProcessorRegistry;
use DemoCode\TimersBundle\TimerSettings\TimerSettings;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;


class DemoCodeTimersExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');

        $this->configureTimers($config, $container);
    }

    private function configureTimers(array $config, ContainerBuilder $container): void
    {
        if (empty($config["timers"])) {
            return;
        }

        $postProcessors = [];
        foreach ($config["timers"] as $className => $methods) {
            foreach ($methods as $methodName => $settings) {
                if (!isset($settings["post_processor"])) {
                    continue;
                }

                $postProcessors[TimerSettings::createIdentifier(new \ReflectionMethod($className, $methodName))] = new Reference($settings["post_processor"]);
            }
        }

        $container->getDefinition(PostProcessorRegistry::class)->replaceArgument(0, $postProcessors);

        $container
            ->getDefinition(TimerPointcut::class)
            ->addTag("jms_aop.pointcut", ["interceptor" => TimerInterceptor::class]);
    }
}
