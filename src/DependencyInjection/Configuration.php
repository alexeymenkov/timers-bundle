<?php

namespace DemoCode\TimersBundle\DependencyInjection;

use DemoCode\TimersBundle\Timer;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Config\Definition\Exception\InvalidTypeException;


class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        /** @var ArrayNodeDefinition $rootNode */
        $rootNode = $treeBuilder->root("demo_code_timers");

        $rootNode
            ->children()
                ->arrayNode("timers")
                    ->useAttributeAsKey('class')
                    ->performNoDeepMerging()
                    ->prototype("array")
                        ->useAttributeAsKey('method')
                        ->prototype("array")
                            ->addDefaultsIfNotSet()
                            ->children()
                                ->scalarNode("group")->defaultValue(Timer::DEFAULT_GROUP)->end()
                                ->variableNode("params")
                                    ->validate()
                                    ->always(function ($v) {
                                        if (!is_array($v)) {
                                            throw new InvalidTypeException();
                                        }

                                        return $v;
                                    })
                                    ->end()
                                ->end()
                                ->scalarNode("post_processor")->end()
                                ->booleanNode("enabled")->defaultTrue()->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
