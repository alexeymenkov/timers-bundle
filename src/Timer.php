<?php


namespace DemoCode\TimersBundle;


class Timer
{
    const DEFAULT_GROUP = 'default';

    /**
     * @var array|TimerEvent[][]
     */
    private $events = [];

    public function start(string $group = self::DEFAULT_GROUP): TimerEvent
    {
        $event = new TimerEvent();
        $this->events[$group][] = $event;

        return $event;
    }

    /**
     * @param string|null $group
     * @return TimerEvent[]|\Iterator
     */
    public function getEvents(string $group = null): \Iterator
    {
        if (empty($this->events) || ($group && !array_key_exists($group, $this->events))) {
            return new \EmptyIterator();
        }

        return $group ? $this->getGroupEventsGenerator($group) : $this->getAllEventsGenerator();
    }

    /**
     * @return string[]
     */
    public function getGroups(): array
    {
        return array_keys($this->events);
    }

    public function reset(array $groups = []): void
    {
        if (!$groups) {
            $this->events = [];
            return;
        }

        foreach ($groups as $group) {
            unset($this->events[$group]);
        }
    }

    /**
     * @return \Iterator|TimerEvent[]
     */
    private function getAllEventsGenerator(): \Iterator
    {
        foreach ($this->events as $group => $events) {
            foreach ($events as $event) {
                yield $event;
            }
        }
    }

    /**
     * @param string $group
     * @return \Iterator|TimerEvent[]
     */
    private function getGroupEventsGenerator(string $group): \Iterator
    {
        foreach ($this->events[$group] as $event) {
            yield $event;
        }
    }
}
