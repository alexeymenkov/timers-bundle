<?php


namespace DemoCode\TimersBundle\Aop\Pointcut;


use DemoCode\TimersBundle\TimerSettings\TimerSettingsStorage;
use JMS\AopBundle\Aop\PointcutInterface;

class TimerPointcut implements PointcutInterface
{
    /**
     * @var TimerSettingsStorage
     */
    private $settingsStorage;

    /**
     * @param TimerSettingsStorage $settingsStorage
     */
    public function __construct(TimerSettingsStorage $settingsStorage)
    {
        $this->settingsStorage = $settingsStorage;
    }

    public function matchesClass(\ReflectionClass $class)
    {
        return true;
    }

    public function matchesMethod(\ReflectionMethod $method)
    {
        return $this->settingsStorage->hasSettings($method);
    }
}