<?php


namespace DemoCode\TimersBundle\Aop\Interceptor;


use CG\Proxy\MethodInterceptorInterface;
use CG\Proxy\MethodInvocation;
use DemoCode\TimersBundle\PostProcessor\PostProcessorRegistry;
use DemoCode\TimersBundle\Timer;
use DemoCode\TimersBundle\TimerSettings\TimerSettings;
use DemoCode\TimersBundle\TimerSettings\TimerSettingsStorage;

class TimerInterceptor implements MethodInterceptorInterface
{
    /**
     * @var TimerSettingsStorage
     */
    private $settingsStorage;

    /**
     * @var Timer
     */
    private $timer;

    /**
     * @var PostProcessorRegistry
     */
    private $postProcessorRegistry;

    /**
     * @param TimerSettingsStorage $settingsStorage
     * @param Timer $timer
     * @param PostProcessorRegistry $postProcessorRegistry
     */
    public function __construct(
        TimerSettingsStorage $settingsStorage,
        Timer $timer,
        PostProcessorRegistry $postProcessorRegistry
    )
    {
        $this->settingsStorage = $settingsStorage;
        $this->timer = $timer;
        $this->postProcessorRegistry = $postProcessorRegistry;
    }

    public function intercept(MethodInvocation $invocation)
    {
        $settings = $this->settingsStorage->getSettings($invocation->reflection);

        $result = null;
        $event = $this->timer->start($settings->getGroup());
        try {
            $result = $invocation->proceed();
        } finally {
            $event->stop();
            $event->setParams(array_merge(["method" => $invocation->reflection->name], $settings->getParams()));

            if ($this->postProcessorRegistry->hasProcessor(TimerSettings::createIdentifier($invocation->reflection))) {
                $processor = $this->postProcessorRegistry->getProcessor(TimerSettings::createIdentifier($invocation->reflection));
                $processor->process($event, $invocation->arguments, $result);
            }
        }

        return $result;
    }
}