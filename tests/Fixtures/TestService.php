<?php


namespace Tests\DemoCode\TimersBundle\Fixtures;


class TestService
{
    public function execute()
    {
        usleep(1000);
    }

    public function run()
    {
        return true;
    }

    public function sendRequest()
    {
        return;
    }
}