<?php


namespace Tests\DemoCode\TimersBundle\Integration;


use DemoCode\TimersBundle\Timer;
use DemoCode\TimersBundle\TimerEvent;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Tests\DemoCode\TimersBundle\Fixtures\TestService;

/**
 * Class IntegrationTest
 * @package Tests\DemoCode\TimersBundle\Integration
 * @coversNothing
 */
class IntegrationTest extends KernelTestCase
{
    protected function setUp()
    {
        self::bootKernel();
    }

    public function testTimers()
    {
        self::$kernel->getContainer()->get(TestService::class)->execute();

        $timer = self::$kernel->getContainer()->get(Timer::class);

        /** @var TimerEvent[] $events */
        $events = iterator_to_array($timer->getEvents());

        expect($events)->count(1);
        expect($events)->hasKey(0);
        expect($events[0]->getDuration())->greaterOrEquals(0.001);
        expect($events[0]->getParams())->equals([
            "service" => "test_service",
            'method' => 'execute'
        ]);
    }
}