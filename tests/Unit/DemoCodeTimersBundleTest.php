<?php

namespace Tests\DemoCode\TimersBundle\Unit;

use DemoCode\TimersBundle\DemoCodeTimersBundle;
use DemoCode\TimersBundle\DependencyInjection\Compiler\TimerSettingsPass;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class DemoCodeTimersBundleTest extends TestCase
{
    /**
     * @test
     */
    public function addsTimerSettingsCompilerPass()
    {
        $bundle = new DemoCodeTimersBundle();
        $container = new ContainerBuilder();
        $bundle->build($container);

        $beforeRemovingPasses = $container->getCompilerPassConfig()->getBeforeRemovingPasses();
        expect($beforeRemovingPasses)->notEmpty();
        expect($beforeRemovingPasses)->hasKey(0);
        expect($beforeRemovingPasses[0])->equals(new TimerSettingsPass());
    }
}
