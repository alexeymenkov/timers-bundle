<?php

namespace Tests\DemoCode\TimersBundle\Unit\Timer;

use DemoCode\TimersBundle\TimerEvent;
use PHPUnit\Framework\TestCase;

class TimerEventTest extends TestCase
{
    public function test()
    {
        $event = new TimerEvent();
        expect($event->getDuration())->null();

        $refClass = new \ReflectionClass(TimerEvent::class);

        $startProperty = $refClass->getProperty("start");
        $startProperty->setAccessible(true);

        usleep(100);
        expect($startProperty->getValue($event))->lessThan(microtime(true));
        expect($event->getStartDate())->isInstanceOf(\DateTime::class);

        $now = new \DateTime();
        expect($event->getStartDate()->getTimezone())->equals($now->getTimezone());
        expect($event->getStartDate()->getTimestamp())->lessOrEquals($now->getTimestamp());

        $endProperty = $refClass->getProperty("end");
        $endProperty->setAccessible(true);

        expect($endProperty->getValue($event))->null();
        expect($event->isStarted())->true();

        expect($event->stop())->true();
        expect($event->stop())->false();

        expect($endProperty->getValue($event))->lessOrEquals(microtime(true));
        expect($event->getDuration())->greaterThan(0.0001);
        expect($event->getDuration())->lessThan(0.1);

        $event->setParams(["service" => "a"]);
        $event->addParam("count", 10);
        expect($event->getParams())->equals([
            "service" => "a",
            "count" => 10,
        ]);
    }
}
