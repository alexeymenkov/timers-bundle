<?php

namespace Tests\DemoCode\TimersBundle\Unit\DependencyInjection\Compiler;

use DemoCode\TimersBundle\DependencyInjection\Compiler\TimerSettingsPass;
use DemoCode\TimersBundle\Timer;
use DemoCode\TimersBundle\TimerSettings\TimerSettings;
use DemoCode\TimersBundle\TimerSettings\TimerSettingsStorage;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\Prophecy\ObjectProphecy;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Tests\DemoCode\TimersBundle\Fixtures\TestService;

class TimerSettingsPassTest extends TestCase
{
    /**
     * @test
     */
    public function savesTimerSettingsFromConfiguration()
    {
        /** @var TimerSettingsStorage|ObjectProphecy $storage */
        $storage = $this->prophesize(TimerSettingsStorage::class);

        $container = new ContainerBuilder();
        $container->set(TimerSettingsStorage::class, $storage->reveal());
        $container->prependExtensionConfig("demo_code_timers", [
            "timers" => [
                TestService::class => [
                    "count" => ["enabled" => false],
                    "execute" => ["group" => "g1"],
                    "run" => ["params" => ["tags" => ["a"]]],
                ],
            ],
        ]);

        $storage->save(Argument::type(TimerSettings::class))->shouldBeCalledTimes(2);

        $pass = new TimerSettingsPass();
        $pass->process($container);

        $calls = $storage->findProphecyMethodCalls("save", new Argument\ArgumentsWildcard([Argument::type(TimerSettings::class)]));
        /** @var TimerSettings $ts1 $ts2 */
        $ts1 = $calls[0]->getArguments()[0];
        expect($ts1->getGroup())->equals("g1");
        expect($ts1->getReflection()->class)->equals(TestService::class);
        expect($ts1->getReflection()->name)->equals("execute");
        expect($ts1->getParams())->equals([]);
        /** @var TimerSettings $ts2 */
        $ts2 = $calls[1]->getArguments()[0];
        expect($ts2->getGroup())->equals(Timer::DEFAULT_GROUP);
        expect($ts2->getReflection()->class)->equals(TestService::class);
        expect($ts2->getReflection()->name)->equals("run");
        expect($ts2->getParams())->equals(["tags" => ["a"]]);
    }
}
