<?php


namespace Tests\DemoCode\TimersBundle\Unit\DependencyInjection;


use DemoCode\TimersBundle\DependencyInjection\Configuration;
use DemoCode\TimersBundle\Timer;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Config\Definition\Exception\InvalidTypeException;
use Symfony\Component\Config\Definition\Processor;
use Tests\DemoCode\TimersBundle\Fixtures\TestService;

class ConfigurationTest extends TestCase
{
    public function configureTimers()
    {
        $config = $this->processConfiguration([
            "timers" => [
                TestService::class => [
                    "run" => [
                        "group" => "a",
                        "params" => ["service" => "b"],
                        "post_processor" => "service_id",
                    ]
                ]
            ]
        ]);

        $expectedConfig = [
            'timers' => [
                TestService::class => [
                    "run" => [
                        "group" => "a",
                        "params" => ["service" => "b"],
                        "post_processor" => "service_id",
                        "enabled" => true,
                    ]
                ]
            ],
        ];

        expect($config)->equals($expectedConfig);
    }

    /**
     * @test
     */
    public function throwsOnInvalidTimerParameters()
    {
        $this->expectException(InvalidTypeException::class);

        $this->processConfiguration([
            "timers" => [
                TestService::class => [
                    "run" => [
                        "params" => "b",
                    ]
                ]
            ]
        ]);
    }

    private function processConfiguration(array $config)
    {
        $configuration = new Configuration();
        $processor = new Processor();

        return $processor->processConfiguration($configuration, ["demo_code_timers" => $config]);
    }
}