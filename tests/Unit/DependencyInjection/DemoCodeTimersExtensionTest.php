<?php


namespace Tests\DemoCode\TimersBundle\Unit\DependencyInjection;


use DemoCode\TimersBundle\Aop\Interceptor\TimerInterceptor;
use DemoCode\TimersBundle\Aop\Pointcut\TimerPointcut;
use DemoCode\TimersBundle\DependencyInjection\DemoCodeTimersExtension;
use DemoCode\TimersBundle\PostProcessor\PostProcessorRegistry;
use DemoCode\TimersBundle\Timer;
use DemoCode\TimersBundle\TimerSettings\CacheTimerSettingsStorage;
use DemoCode\TimersBundle\TimerSettings\TimerSettings;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;
use Tests\DemoCode\TimersBundle\Fixtures\TestService;

class DemoCodeTimersExtensionTest extends TestCase
{
    /**
     * @test
     */
    public function exposesServices()
    {
        $container = $this->loadExtension();

        expect($container->hasDefinition(Timer::class))->true();
        expect($container->hasDefinition("demo_code.timer_settings_cache"))->true();
        expect($container->hasDefinition(CacheTimerSettingsStorage::class))->true();
        expect($container->hasDefinition(PostProcessorRegistry::class))->true();
        expect($container->hasDefinition(TimerInterceptor::class))->true();
        expect($container->hasDefinition(TimerPointcut::class))->true();

        expect($container->getDefinition(Timer::class)->isPublic())->true();
        expect($container->getDefinition(TimerInterceptor::class)->isPublic())->true();
        expect($container->getDefinition(TimerPointcut::class)->isPublic())->true();

        expect($container->getDefinition(TimerPointcut::class)->hasTag("jms_aop.pointcut"))->false();
        expect($container->getDefinition(PostProcessorRegistry::class)->getArguments())->equals([
            0 => [],
        ]);
    }

    /**
     * @test
     */
    public function configuresTimers()
    {
        $container = $this->loadExtension([
            "timers" => [
                TestService::class => [
                    "execute" => [
                        "post_processor" => "service_id"
                    ],
                    "run" => [
                    ],
                    "sendRequest" => [
                        "post_processor" => "service_id"
                    ]
                ]
            ]
        ]);

        expect($container->getDefinition(TimerPointcut::class)->hasTag("jms_aop.pointcut"))->true();
        expect($container->getDefinition(TimerPointcut::class)->getTag("jms_aop.pointcut"))->equals([
            ["interceptor" => TimerInterceptor::class]
        ]);

        $identifier1 = TimerSettings::createIdentifier(new \ReflectionMethod(TestService::class, "execute"));
        $identifier2 = TimerSettings::createIdentifier(new \ReflectionMethod(TestService::class, "sendRequest"));
        expect($container->getDefinition(PostProcessorRegistry::class)->getArgument(0))->equals([
            $identifier1 => new Reference("service_id"),
            $identifier2 => new Reference("service_id"),
        ]);
    }

    /**
     * @param array $config
     * @return ContainerBuilder
     */
    protected function loadExtension(array $config = [])
    {
        $container = new ContainerBuilder();
        $config = [
            'demo_code_timers' => $config
        ];

        (new DemoCodeTimersExtension())->load($config, $container);

        return $container;
    }
}
