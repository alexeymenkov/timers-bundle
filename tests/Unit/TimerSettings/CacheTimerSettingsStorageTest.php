<?php

namespace Tests\DemoCode\TimersBundle\Unit\TimerSettings;

use DemoCode\TimersBundle\Timer;
use DemoCode\TimersBundle\TimerSettings\CacheTimerSettingsStorage;
use DemoCode\TimersBundle\TimerSettings\TimerSettings;
use Doctrine\Common\Cache\ArrayCache;
use PHPUnit\Framework\TestCase;
use Tests\DemoCode\TimersBundle\Fixtures\TestService;

class CacheTimerSettingsStorageTest extends TestCase
{
    /**
     * @var ArrayCache
     */
    private $cache;
    /**
     * @var \ReflectionMethod
     */
    private $reflection;
    /**
     * @var CacheTimerSettingsStorage
     */
    private $storage;

    protected function setUp()
    {
        $this->cache = new ArrayCache();
        $this->reflection = new \ReflectionMethod(TestService::class, "execute");
        $this->storage = new CacheTimerSettingsStorage($this->cache);
    }

    /**
     * @test
     */
    public function saveSettings()
    {
        $timerSettings = new TimerSettings($this->reflection);

        expect($this->storage->save($timerSettings))->true();

        $identifier = TimerSettings::createIdentifier($this->reflection);

        /** @var TimerSettings $settings */
        $settings = $this->cache->fetch($identifier);
        expect($settings)->isInstanceOf(TimerSettings::class);
        expect($settings->getGroup())->equals(Timer::DEFAULT_GROUP);
        expect($settings->getIdentifier())->equals($identifier);
        expect($settings->getParams())->equals([]);
        expect($settings->getReflection())->same($this->reflection);
    }

    /**
     * @test
     */
    public function hasCacheSettings()
    {
        $this->cache->save(
            TimerSettings::createIdentifier($this->reflection),
            new TimerSettings($this->reflection)
        );

        expect($this->storage->hasSettings($this->reflection))->true();
        expect($this->storage->hasSettings(new \ReflectionMethod(TestService::class, "run")))->false();
    }

    /**
     * @test
     */
    public function getCacheSettings()
    {
        $cacheSettings = new TimerSettings($this->reflection);
        $this->cache->save(TimerSettings::createIdentifier($this->reflection), $cacheSettings);

        expect($this->storage->getSettings($this->reflection))->same($cacheSettings);
    }

    /**
     * @test
     */
    public function throwsWhenCacheSettingsNotExist()
    {
        $this->expectException(\InvalidArgumentException::class);

        $this->storage->getSettings($this->reflection);
    }
}
