<?php

namespace Tests\DemoCode\TimersBundle\Unit\TimerSettings;

use DemoCode\TimersBundle\TimerSettings\TimerSettings;
use PHPUnit\Framework\TestCase;
use Tests\DemoCode\TimersBundle\Fixtures\TestService;

class TimerSettingsTest extends TestCase
{
    public function testSerialization()
    {
        $settings = new TimerSettings(
            new \ReflectionMethod(TestService::class, "execute"),
            "test",
            ["a" => 1, "b" => "foo"]
        );

        $serialized = serialize($settings);
        $deserialized = unserialize($serialized);

        expect($deserialized)->equals($settings);
    }
}
