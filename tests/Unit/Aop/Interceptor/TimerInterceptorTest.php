<?php

namespace Tests\DemoCode\TimersBundle\Unit\Aop\Interceptor;

use CG\Proxy\MethodInvocation;
use DemoCode\TimersBundle\Aop\Interceptor\TimerInterceptor;
use DemoCode\TimersBundle\PostProcessor\PostProcessor;
use DemoCode\TimersBundle\PostProcessor\PostProcessorRegistry;
use DemoCode\TimersBundle\Timer;
use DemoCode\TimersBundle\TimerEvent;
use DemoCode\TimersBundle\TimerSettings\TimerSettings;
use DemoCode\TimersBundle\TimerSettings\TimerSettingsStorage;
use PHPUnit\Framework\TestCase;
use Prophecy\Prophecy\ObjectProphecy;

class TimerInterceptorTest extends TestCase
{
    /**
     * @var ObjectProphecy|TimerSettingsStorage
     */
    private $settingsStorage;
    /**
     * @var ObjectProphecy|Timer
     */
    private $timer;
    /**
     * @var ObjectProphecy|PostProcessorRegistry
     */
    private $postProcessorRegistry;
    /**
     * @var TimerInterceptor
     */
    private $interceptor;

    protected function setUp()
    {
        $this->settingsStorage = $this->prophesize(TimerSettingsStorage::class);
        $this->timer = $this->prophesize(Timer::class);
        $this->postProcessorRegistry = $this->prophesize(PostProcessorRegistry::class);
        $this->interceptor = new TimerInterceptor(
            $this->settingsStorage->reveal(),
            $this->timer->reveal(),
            $this->postProcessorRegistry->reveal()
        );
    }

    /**
     * @test
     */
    public function calculatesPublicMethodInvocationTime()
    {
        $invocation = $this->createInvocation("calculate");

        $ts = new TimerSettings($invocation->reflection, "some_group", ["data" => 2]);
        $this->settingsStorage->getSettings($invocation->reflection)->shouldBeCalled()->willReturn($ts);

        $event = new TimerEvent();
        $this->timer->start("some_group")->shouldBeCalled()->willReturn($event);
        $this->postProcessorRegistry
            ->hasProcessor(TimerSettings::createIdentifier($invocation->reflection))
            ->shouldBeCalled()
            ->willReturn(false);

        expect($this->interceptor->intercept($invocation))->equals(20);

        expect($event->isStarted())->false();
        expect($event->getDuration())->greaterOrEquals(0.001);
        expect($event->getParams()["method"])->equals("calculate");
        expect($event->getParams())->hasKey("data");
        expect($event->getParams()["data"])->equals(2);
    }

    /**
     * @test
     */
    public function calculatesProtectedMethodInvocationTime()
    {
        $invocation = $this->createInvocation("protectedMethod");

        $event = new TimerEvent();
        $this->configureMockObjects($invocation, $event);

        expect($this->interceptor->intercept($invocation))->equals("protected");

        expect($event->isStarted())->false();
        expect($event->getDuration())->greaterOrEquals(0.001);
        expect($event->getParams()["method"])->equals("protectedMethod");
    }

    /**
     * @test
     */
    public function calculatesPrivateMethodInvocationTime()
    {
        $invocation = $this->createInvocation("privateMethod");

        $event = new TimerEvent();
        $this->configureMockObjects($invocation, $event);

        expect($this->interceptor->intercept($invocation))->equals("private");
        expect($event->isStarted())->false();
        expect($event->getDuration())->greaterOrEquals(0.001);
        expect($event->getParams()["method"])->equals("privateMethod");
    }

    /**
     * @test
     */
    public function calculatesMethodInvocationTimeOnException()
    {
        $invocation = $this->createInvocation("throwException");

        $event = new TimerEvent();
        $this->configureMockObjects($invocation, $event);

        $e = null;
        try {
            $this->interceptor->intercept($invocation);
        } catch (InvocationException $e) {}

        expect($e)->isInstanceOf(InvocationException::class);
        expect($e->getMessage())->equals("Invocation failed");

        expect($event->isStarted())->false();
        expect($event->getDuration())->greaterOrEquals(0.001);
        expect($event->getParams()["method"])->equals("throwException");
    }

    /**
     * @test
     */
    public function runsPostProcessor()
    {
        /** @var PostProcessor|ObjectProphecy $postProcessor */
        $postProcessor = $this->prophesize(PostProcessor::class);

        $invocation = $this->createInvocation("calculate");

        $ts = new TimerSettings($invocation->reflection);
        $this->settingsStorage->getSettings($invocation->reflection)->shouldBeCalled()->willReturn($ts);

        $event = new TimerEvent();
        $this->timer->start(Timer::DEFAULT_GROUP)->shouldBeCalled()->willReturn($event);

        $postProcessor->process($event, [10], 20)->shouldBeCalled();
        $this->postProcessorRegistry
            ->hasProcessor(TimerSettings::createIdentifier($invocation->reflection))
            ->shouldBeCalled()
            ->willReturn(true);

        $this->postProcessorRegistry
            ->getProcessor(TimerSettings::createIdentifier($invocation->reflection))
            ->shouldBeCalled()
            ->willReturn($postProcessor->reveal());

        expect($this->interceptor->intercept($invocation))->equals(20);
    }

    private function createInvocation(string $methodName): MethodInvocation
    {
        return new MethodInvocation(new \ReflectionMethod(TestClass::class, $methodName), new TestClass(), [10], []);
    }

    private function configureMockObjects(MethodInvocation $invocation, TimerEvent $event)
    {
        $ts = new TimerSettings($invocation->reflection);
        $this->settingsStorage->getSettings($invocation->reflection)->shouldBeCalled()->willReturn($ts);

        $this->timer->start(Timer::DEFAULT_GROUP)->shouldBeCalled()->willReturn($event);

        $this->postProcessorRegistry
            ->hasProcessor(TimerSettings::createIdentifier($invocation->reflection))
            ->shouldBeCalled()
            ->willReturn(false);
    }

}

class TestClass
{
    public function calculate(int $num)
    {
        usleep(1000);

        return $num * 2;
    }

    public function throwException()
    {
        usleep(1000);

        throw new InvocationException();
    }

    protected function protectedMethod()
    {
        usleep(1000);

        return "protected";
    }

    private function privateMethod()
    {
        usleep(1000);

        return "private";
    }
}

class InvocationException extends \Exception
{
    protected $message = "Invocation failed";
}