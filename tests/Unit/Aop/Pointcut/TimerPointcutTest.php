<?php

namespace Tests\DemoCode\TimersBundle\Unit\Aop\Pointcut;

use DemoCode\TimersBundle\Aop\Interceptor\TimerInterceptor;
use DemoCode\TimersBundle\Aop\Pointcut\TimerPointcut;
use DemoCode\TimersBundle\TimerSettings\TimerSettingsStorage;
use PHPUnit\Framework\TestCase;
use Prophecy\Prophecy\ObjectProphecy;

class TimerPointcutTest extends TestCase
{
    /**
     * @test
     * @dataProvider matchesAnyClassExamples
     * @param \ReflectionClass $reflectionClass
     */
    public function matchesAnyClass(\ReflectionClass $reflectionClass)
    {
        $storage = $this->prophesize(TimerSettingsStorage::class);

        $pointcut = new TimerPointcut($storage->reveal());
        expect($pointcut->matchesClass($reflectionClass))->true();
    }

    public function matchesAnyClassExamples()
    {
        return [
            [
                new \ReflectionClass(__CLASS__)
            ],
            [
                new \ReflectionClass(TimerInterceptor::class),
            ],
            [
                new \ReflectionClass(\AppKernel::class),
            ]
        ];
    }

    /**
     * @test
     * @param bool $hasSettings
     * @param bool $expected
     *
     * @dataProvider matchesMethodWithSettingsExamples
     */
    public function matchesMethodWithSettings(bool $hasSettings, bool $expected)
    {
        /** @var TimerSettingsStorage|ObjectProphecy $storage */
        $storage = $this->prophesize(TimerSettingsStorage::class);
        $reflection = new \ReflectionMethod(__METHOD__);

        $storage->hasSettings($reflection)->shouldBeCalled()->willReturn($hasSettings);

        $pointcut = new TimerPointcut($storage->reveal());
        expect($pointcut->matchesMethod($reflection))->equals($expected);
    }

    public function matchesMethodWithSettingsExamples()
    {
        return [
            [true, true],
            [false, false],
        ];
    }
}
