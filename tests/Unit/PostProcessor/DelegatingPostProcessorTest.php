<?php

namespace Tests\DemoCode\TimersBundle\Unit\PostProcessor;

use DemoCode\TimersBundle\PostProcessor\DelegatingPostProcessor;
use DemoCode\TimersBundle\PostProcessor\PostProcessor;
use DemoCode\TimersBundle\TimerEvent;
use PHPUnit\Framework\TestCase;
use Prophecy\Prophecy\ObjectProphecy;

class DelegatingPostProcessorTest extends TestCase
{
    /**
     * @test
     */
    public function delegateProcessing()
    {
        /** @var PostProcessor|ObjectProphecy $p1 */
        $p1 = $this->prophesize(PostProcessor::class);
        /** @var PostProcessor|ObjectProphecy $p2 */
        $p2 = $this->prophesize(PostProcessor::class);
        $event = new TimerEvent();
        $p1->process($event, ["args"], "result")->shouldBeCalled();
        $p2->process($event, ["args"], "result")->shouldBeCalled();

        $delegatingProcessor = new DelegatingPostProcessor([$p1->reveal(), $p2->reveal()]);
        $delegatingProcessor->process($event, ["args"], "result");
    }
}
