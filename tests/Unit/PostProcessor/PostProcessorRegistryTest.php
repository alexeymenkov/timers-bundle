<?php

namespace Tests\DemoCode\TimersBundle\Unit\PostProcessor;

use DemoCode\TimersBundle\PostProcessor\PostProcessor;
use DemoCode\TimersBundle\PostProcessor\PostProcessorRegistry;
use PHPUnit\Framework\TestCase;

class PostProcessorRegistryTest extends TestCase
{
    /**
     * @test
     */
    public function getProcessor()
    {
        $p1 = $this->prophesize(PostProcessor::class)->reveal();
        $p2 = $this->prophesize(PostProcessor::class)->reveal();

        $registry = new PostProcessorRegistry(["p1" => $p1, "p2" => $p2]);
        expect($registry->getProcessor("p1"))->equals($p1);
        expect($registry->getProcessor("p2"))->equals($p2);
    }

    /**
     * @test
     */
    public function hasProcessor()
    {
        $p1 = $this->prophesize(PostProcessor::class)->reveal();

        $registry = new PostProcessorRegistry(["p1" => $p1]);
        expect($registry->hasProcessor("p1"))->true();
        expect($registry->hasProcessor("p2"))->false();
    }

    /**
     * @test
     */
    public function throwsWhenKeyIsNotString()
    {
        $this->expectException(\TypeError::class);

        new PostProcessorRegistry([$this->prophesize(PostProcessor::class)->reveal()]);
    }

    /**
     * @test
     */
    public function throwsWhenNonPostProcessorGiven()
    {
        $this->expectException(\TypeError::class);

        new PostProcessorRegistry([new \stdClass()]);
    }

    /**
     * @test
     */
    public function throwsWhenProcessorNotExists()
    {
        $this->expectException(\InvalidArgumentException::class);
        $p1 = $this->prophesize(PostProcessor::class)->reveal();

        $registry = new PostProcessorRegistry(["p1" => $p1]);

        $registry->getProcessor("p2");
    }
}
