<?php

namespace Tests\DemoCode\TimersBundle\Unit\Timer;

use DemoCode\TimersBundle\Timer;
use DemoCode\TimersBundle\TimerEvent;
use PHPUnit\Framework\TestCase;

class TimerTest extends TestCase
{
    /**
     * @test
     */
    public function startAndReturnsTimerEvent()
    {
        $timer = new Timer();
        $event = $timer->start();

        expect($event)->isInstanceOf(TimerEvent::class);
        expect($event->isStarted())->true();
    }

    /**
     * @test
     */
    public function returnsTimerGroups()
    {
        $timer = new Timer();
        $timer->start();
        $timer->start("group1");

        expect($timer->getGroups())->equals([Timer::DEFAULT_GROUP, "group1"]);
    }

    /**
     * @test
     * @dataProvider returnsEventsExamples
     *
     * @param Timer $timer
     * @param array $expectedEvents
     */
    public function returnsEvents(Timer $timer, $group, array $expectedEvents)
    {
        $events = $timer->getEvents($group);
        expect($events)->isInstanceOf(\Iterator::class);
        $eventList = iterator_to_array($events);
        expect($eventList)->equals($expectedEvents);
    }

    public function returnsEventsExamples()
    {
        $timer = new Timer();
        $event1 = $timer->start();
        $event2 = $timer->start();
        $event3 = $timer->start("group1");

        return [
            [
                new Timer(), null, [],
            ],
            [
                $timer, "other", [],
            ],
            [
                $timer, null, [$event1, $event2, $event3],
            ],
            [
                $timer, Timer::DEFAULT_GROUP, [$event1, $event2],
            ],
            [
                $timer, "group1", [$event3],
            ],
        ];
    }

    /**
     * @test
     * @dataProvider resetsEventsExamples
     *
     * @param array $timerGroups
     * @param array $resetGroups
     * @param int $expectedCount
     */
    public function resetsEvents(array $timerGroups, array $resetGroups, int $expectedCount)
    {
        $timer = new Timer();
        foreach ($timerGroups as $group) {
            $timer->start($group);
        }

        expect(iterator_to_array($timer->getEvents()))->count(count($timerGroups));

        $timer->reset($resetGroups);
        expect(iterator_to_array($timer->getEvents()))->count($expectedCount);
        expect(array_intersect($timer->getGroups(), $resetGroups))->count(0);
    }

    public function resetsEventsExamples()
    {
        return [
            [
                [Timer::DEFAULT_GROUP, Timer::DEFAULT_GROUP, 'group_1'], [], 0
            ],
            [
                [Timer::DEFAULT_GROUP, Timer::DEFAULT_GROUP, 'group_1'], ['group_1'], 2
            ],
            [
                [Timer::DEFAULT_GROUP, 'group_1', 'group_2'], ['group_1', 'group_2'], 1
            ],
            [
                ['group_1', 'group_2'], ['group_1', 'group_2'], 0
            ],
        ];
    }
}
